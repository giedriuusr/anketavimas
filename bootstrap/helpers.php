<?php

use App\Models\AdminEmail;
use App\Models\Auth\User;
use App\Services\CountriesManager;

/**
 * @param null|string $iso
 * @return CountriesManager|string|array
 */
function countries($iso = null)
{
    /** @var CountriesManager $manager */
    $manager = app(CountriesManager::class);

    if ($iso) {
        return $manager->get($iso);
    }

    return $manager->all();
}

function user() {
    /** @var \App\Models\Auth\Authenticatable $user */
    $user = auth()->user();

    return $user->hydrateToRealModel();
}

/**
 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
 */
function _user() {
    /** @var \App\Models\Auth\Authenticatable $user */
    $user = auth()->user();

    return User::query()->find($user->id);
}

/**
 * @param $amount
 * @param string $currency
 *
 * @return string
 */
function amountToWords($amount, $currency = 'EUR') {
    $euros = floor($amount);
    $cents = round(($amount - $euros) * 100);

    $number = NumberFormatter::create('lt', NumberFormatter::SPELLOUT);

    return "{$number->format($euros)} {$currency} ir {$cents} ct.";
}

/**
 * @return array
 */
function adminEmails() {
    return AdminEmail::query()->where('is_active', true)->pluck('email')->toArray();
}
