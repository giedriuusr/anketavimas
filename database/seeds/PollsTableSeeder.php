<?php

use App\Models\Poll;
use App\Models\PollQuestion;
use App\Models\PollQuestionOption;
use App\Models\PollResult;
use App\Models\PollResultAnswer;
use Illuminate\Database\Seeder;

class PollsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Poll::class, 10)
            ->create()
            ->each(function ($poll) {
                /** @var Poll $poll */
                $questions = $poll->questions()->saveMany(factory(PollQuestion::class, rand(2, 3))->make());

                /** @var PollQuestion $question */
                foreach ($questions as $question) {
                    if ($this->notText($question)) {
                        $question->options()->saveMany(factory(PollQuestionOption::class, rand(2, 5))->make());

                        continue;
                    }

                    $question->options()->save(factory(PollQuestionOption::class)->make());
                }

                factory(PollResult::class, rand(1, 10))->create([
                    'poll_id' =>  $poll->id,
                ])->each(function ($result) use ($questions) {
                    foreach ($questions as $question) {
                        /** @var PollResult $result */
                        $result->answers()->save(factory(PollResultAnswer::class)->make([
                            'question_id' => $question->id,
                            'option_id' => $question->options->random()->id,
                        ]));
                    }
                });
            });
    }

    /**
     * @param $question
     *
     * @return bool
     */
    public function notText($question) {
        return $question->type == PollQuestion::TYPE_CHECKBOX || $question->type == PollQuestion::TYPE_RADIO;
    }
}
