<?php

use App\Models\Auth\Admin;
use App\Models\Auth\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Admin::class)->create([
            'first_name' => 'Test',
            'last_name' => 'Administrator',
            'email' => 'admin@example.com',
            'email_verified_at' => now(),
        ]);

        factory(User::class)->create([
            'first_name' => 'Giedrius',
            'last_name' => 'Rimkus',
            'email' => 'giedrius@example.com',
            'email_verified_at' => now(),
        ]);
    }
}
