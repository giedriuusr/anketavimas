<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollResultAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poll_result_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('result_id');
            $table->foreign('result_id')->references('id')->on('poll_results')->onDelete('cascade');
            $table->unsignedInteger('question_id');
            $table->foreign('question_id')->references('id')->on('poll_questions')->onDelete('cascade');
            $table->unsignedInteger('option_id')->nullable();
            $table->foreign('option_id')->references('id')->on('poll_question_options')->onDelete('cascade');
            $table->text('value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poll_result_answers');
    }
}
