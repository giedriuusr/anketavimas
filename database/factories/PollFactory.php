<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Auth\User;
use App\Models\Poll;
use App\Models\PollQuestion;
use App\Models\PollQuestionOption;
use App\Models\PollResult;
use App\Models\PollResultAnswer;
use Faker\Generator as Faker;

$factory->define(Poll::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'status' => $faker->randomElement([
            Poll::STATUS_ACTIVE,
            Poll::STATUS_INACTIVE,
            Poll::STATUS_DRAFT
        ]),
        'starts_at' => now()->toDateString(),
        'ends_at' => now()->addWeek()->toDateString(),
    ];
});

$factory->define(PollQuestion::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'type' => $faker->randomElement([
            PollQuestion::TYPE_RADIO,
            PollQuestion::TYPE_CHECKBOX,
            PollQuestion::TYPE_TEXT,
            PollQuestion::TYPE_TEXTAREA,
        ]),
        'poll_id' => factory(Poll::class),
        'required' => $faker->randomElement([0, 1]),
    ];
});

$factory->define(PollQuestionOption::class, function (Faker $faker) {
    return [
        'value' => $faker->sentence,
        'question_id' => factory(PollQuestion::class),
    ];
});

$factory->define(PollResult::class, function (Faker $faker) {
    return [
        'submitted_at' => $faker->dateTime(),
        'user_id' => factory(User::class),
    ];
});

$factory->define(PollResultAnswer::class, function (Faker $faker) {
    return [
        'value' => $faker->sentence,
    ];
});
