@extends('app')

@section('content')
    <div class="row align-items-center justify-content-center" style="height: 100vh">
        <div class="col-xl-4 col-12">
            <h2 class="text-center">{{ __('Reset Password') }}</h2>

            <form method="POST"
                  action="{{ route('password.request') }}"
                  aria-label="{{ __('Reset Password') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group">
                    <label for="email">{{ __('Email') }}</label>

                    <input id="email" type="email"
                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" value="{{ $email ?? old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <strong class="invalid-feedback" role="alert">{{ $errors->first('email') }}</strong>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password">{{ __('New Password') }}</label>

                    <input id="password"
                           type="password"
                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password"
                           required
                    >

                    @if ($errors->has('password'))
                        <strong class="invalid-feedback" role="alert">{{ $errors->first('password') }}</strong>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password-confirm">{{ __('Confirm New Password') }}</label>

                    <input
                        id="password-confirm"
                        type="password"
                        class="form-control"
                        name="password_confirmation"
                        required
                    >
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary w-100">{{ __('Reset Password') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
