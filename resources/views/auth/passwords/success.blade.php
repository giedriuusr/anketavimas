@extends('app')

@section('content')
    <div class="row align-items-center justify-content-center" style="height: 100vh">
        <div class="col-sm-6 text-center">
            <h2 class="text-center">{{ __('Reset password') }}</h2>

            <p>{{ __('You can now login!') }}</p>

            <a class="btn btn-primary" href="/">{{ __('Login') }}</a>
        </div>
    </div>
@endsection
