import Vue from 'vue'
import Vuex from 'vuex'
import User from '*/models/User'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: new User({}),
    poll: {}
  },

  getters: {
    user (state) {
      return state.user
    },

    poll (state) {
      return state.poll
    }
  },

  mutations: {
    setUser (state, payload) {
      state.user = payload
    },

    resetUser (state) {
      state.user = new User({})
    },

    setPoll (state, payload) {
      state.poll = payload
    },

    resetPoll (state) {
      state.poll = {}
    },

    addPollQuestion (state) {
      state.poll.questions.push({
        options: []
      })
    }
  }
})
