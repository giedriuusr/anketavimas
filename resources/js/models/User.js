import _ from 'lodash'

export default class User {
  id = null
  email = null
  firstName = null
  lastName = null
  fullName = null
  isAdmin = false
  type = false
  isVerified = false

  constructor (data) {
    _.forEach(data, (value, field) => {
      if (this.hasOwnProperty(field)) {
        this[field] = value
      }
    })
  }
}
