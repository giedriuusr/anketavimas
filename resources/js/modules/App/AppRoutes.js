import Dashboard from '@/Dashboard/Dashboard'
import auth from '*/middleware/auth'
import verified from '*/middleware/verified'

export const appRoutes = [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      middleware: [auth, verified]
    }
  }
]
