import LoginComponent from '@/Auth/Login'
import Register from '@/Auth/Register/Register'
import PasswordReset from '@/Auth/PasswordReset'
import guest from '*/middleware/guest'
import Verify from '@/Auth/Verify'
import auth from '*/middleware/auth'
import notVerified from '*/middleware/notVerified'

export const authRoutes = [
  {
    path: '/login',
    name: 'login',
    component: LoginComponent,
    meta: {
      middleware: [guest]
    }
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
    meta: {
      middleware: [guest]
    }
  },
  {
    path: '/password-reset',
    name: 'password-reset',
    component: PasswordReset,
    meta: {
      middleware: [guest]
    }
  },
  {
    path: '/verify',
    name: 'verify',
    component: Verify,
    meta: {
      middleware: [auth, notVerified]
    }
  }
]
