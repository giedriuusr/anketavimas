import admin from '*/middleware/admin'
import auth from '*/middleware/auth'
import client from '*/middleware/client'
import ActivePolls from '@/Polls/ActivePolls'
import PollResults from '@/Polls/Admin/PollResults'
import PollsCreate from '@/Polls/Admin/PollsCreate'
import PollsEdit from '@/Polls/Admin/PollsEdit'
import PollsList from '@/Polls/Admin/PollsList'
import AnswerPoll from '@/Polls/AnswerPoll'

export const pollsRoutes = [
  {
    path: '/admin/polls',
    name: 'admin.polls',
    component: PollsList,
    meta: {
      middleware: [admin, auth]
    }
  },
  {
    path: '/admin/polls/:id/edit',
    name: 'admin.polls.edit',
    component: PollsEdit,
    meta: {
      middleware: [admin, auth]
    }
  },
  {
    path: '/admin/polls/create',
    name: 'admin.polls.create',
    component: PollsCreate,
    meta: {
      middleware: [admin, auth]
    }
  },
  {
    path: '/polls',
    name: 'polls',
    component: ActivePolls,
    meta: {
      middleware: [client, auth]
    }
  },
  {
    path: '/poll/:slug',
    name: 'poll',
    component: AnswerPoll,
    meta: {
      middleware: [client, auth]
    }
  },
  {
    path: '/admin/polls/:id/results',
    name: 'polls.results',
    component: PollResults,
    meta: {
      middleware: [admin, auth]
    }
  }
]
