import ProfileEdit from '@/Profile/ProfileEdit'
import auth from '*/middleware/auth'
import verified from '*/middleware/verified'

export const profileRoutes = [
  {
    path: '/profile',
    name: 'profile',
    component: ProfileEdit,
    meta: {
      middleware: [auth, verified]
    }
  }
]
