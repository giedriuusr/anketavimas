import Vue from 'vue'
import Toasted from 'vue-toasted'
Vue.use(Toasted)

window.successToast = {
  type: 'success',
  duration: 5000,
  icon: {
    name: 'check'
  }
}

window.errorToast = {
  type: 'error',
  duration: 5000,
  icon: {
    name: 'error'
  }
}

Vue.axios.interceptors.response.use((response) => {
  let useToaster = response.config.toaster

  if (response.data.message) {
    if (useToaster !== false) {
      Vue.toasted.show(response.data.message, successToast)
    }

    return response
  }

  return response
}, (error) => {
  if ([401, 403].includes(error.response.status)) {
    Vue.router.push('/login')
  }

  let useToaster = error.response.config.toaster

  if (useToaster !== false) {
    let message = error.response.data.message
    Vue.toasted.show(message, errorToast)
  }

  return Promise.reject(error)
})
