import Datepicker from '*/plugins/Datepicker'
import _ from 'lodash'
import Modal from './Modal'
import Datatable from '@western-investment/vue-datatable'

export default {
  install (Vue) {
    Vue.use(Datatable, {
      lang: {
        nextText: 'Kitas',
        prevText: 'Atgal',
        noDataText: 'Nėra anketų'
      }
    })

    Vue.component('modal', Modal)
    Vue.component('Datepicker', Datepicker)

    Vue.prototype.setErrorsFromResponse = function (response) {
      if (response.response.status === 422) {
        let errors = response.response.data.errors

        _.each(errors, (error, field) => {
          this.errors.add({ field, msg: error[0] })
        })
      }
    }

    Vue.prototype.$validationError = function () {
      Vue.toasted.show('Įvesti duomenys neteisingi.', errorToast)
    }
  }
}
