import User from '+/User'
import Vue from 'vue'
import store from '*/store'
import AppComponent from '@/App/App'
import VueAxios from 'vue-axios'
import axios from 'axios'
import Plugins from '*/plugins/plugins'
import VeeValidate from 'vee-validate'
import ApiPlugin from '*/services/api'
import { loadProgressBar } from 'axios-progress-bar'
import router from '*/router'
import VueAdmin from '@western-investment/vue-admin'
import 'axios-progress-bar/dist/nprogress.css'

require('./bootstrap')
require('./icons')
require('chart.js')

window.Vue = Vue
Vue.use(VueAdmin)
Vue.use(VeeValidate)

Vue.router = router

window.Vue = Vue
Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = '/api'
Vue.use(ApiPlugin)

require('./plugins/toaster')

Vue.use(Plugins)
loadProgressBar()

Vue.use(require('@websanova/vue-auth'), {
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  parseUserData: function (data) {
    return new User(data.data)
  }
})

window.lodash = Vue.prototype._ = _

function nextFactory (context, middleware, index) {
  const subsequentMiddleware = middleware[index]
  if (!subsequentMiddleware) return context.next
  return () => {
    const nextMiddleware = nextFactory(context, middleware, index + 1)
    subsequentMiddleware({ ...context, next: nextMiddleware })
  }
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware]

    const context = {
      from,
      next,
      router,
      to
    }

    const nextMiddleware = nextFactory(context, middleware, 1)
    return middleware[0]({ ...context, next: nextMiddleware })
  }

  return next()
})

new Vue({
  router,
  store,
  el: '#app',
  render: (h) => h(AppComponent)
})
