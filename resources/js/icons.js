import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faEdit,
  faSortUp,
  faSortDown,
  faUserAlt,
  faGem,
  faEye
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Vue from 'vue'

library.add([
  faEdit,
  faSortUp,
  faSortDown,
  faUserAlt,
  faGem,
  faEye,
])

Vue.component('font-awesome-icon', FontAwesomeIcon)
