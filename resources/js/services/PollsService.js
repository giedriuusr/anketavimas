import Vue from 'vue'

export default {
  admin: {
    list (params = []) {
      return Vue.axios.get(`/admin/polls`, { params })
        .then((response) => response.data)
    },

    manage (pollId, data) {
      return pollId
        ? Vue.axios.put(`/admin/polls/${pollId}`, data)
        : Vue.axios.post(`/admin/polls`, data)
    },

    show (pollId) {
      return Vue.axios.get(`/admin/polls/${pollId}`)
        .then((response) => response.data)
    }
  },
  user: {
    list (params = []) {
      return Vue.axios.get(`/polls`, { params })
        .then((response) => response.data)
    },

    answer (slug, data) {
      return Vue.axios.post(`/poll/${slug}`, data)
    },

    show (slug) {
      return Vue.axios.get(`/poll/${slug}`)
        .then((response) => response.data)
    }
  }
}
