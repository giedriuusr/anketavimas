import Vue from 'vue'

export default {
  register (data) {
    return Vue.axios.post('/register', data)
  },

  resetPassword (email) {
    return Vue.axios.post('/password-reset', { 'email': email })
  },

  updateProfile (data) {
    return Vue.axios.put('/auth/profile', data)
  },

  changePassword (data) {
    return Vue.axios.put('/auth/change-password', data)
  },

  resendVerification () {
    return Vue.axios.get('/email/resend')
  },

  locale (userId, locale) {
    return Vue.axios.put(`/auth/${userId}/locale`, { locale })
  }
}
