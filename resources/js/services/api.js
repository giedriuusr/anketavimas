import PollsService from '%/PollsService'
import AuthService from '*/services/AuthService'

export const $api = {
  auth: AuthService,
  polls: PollsService
}

const ApiPlugin = {
  install (Vue) {
    Vue.prototype.$api = $api
  }
}

export default ApiPlugin
