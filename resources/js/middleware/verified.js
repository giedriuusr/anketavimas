import Vue from 'vue'

export default function verified ({ next, router }) {
  if (!Vue.auth.user().isVerified) {
    return router.push({ name: 'verify' })
  }

  return next()
}
