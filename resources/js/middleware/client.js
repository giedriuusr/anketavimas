import Vue from 'vue'

export default function client ({ next, router }) {
  let isAdmin = Vue.auth.user().isAdmin

  if (isAdmin) {
    return router.push('/')
  }

  return next()
}
