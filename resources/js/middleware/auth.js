export default function auth ({ next, router }) {
  if (!localStorage.getItem('default_auth_token')) {
    return router.push({ name: 'login' })
  }

  return next()
}
