export default function guest ({ next, router }) {
  if (localStorage.getItem('default_auth_token')) {
    return router.push('/')
  }

  return next()
}
