import Vue from 'vue'

export default function notVerified ({ next, router }) {
  if (Vue.auth.user().isVerified) {
    return router.push({ name: 'dashboard' })
  }

  return next()
}
