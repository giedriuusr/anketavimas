import Vue from 'vue'

export default function admin ({ next, router }) {
  let isAdmin = Vue.auth.user().isAdmin

  if (!isAdmin) {
    return router.push('/')
  }

  return next()
}
