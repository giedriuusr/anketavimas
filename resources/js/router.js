import Vue from 'vue'
import Router from 'vue-router'
import { appRoutes } from '@/App/AppRoutes'
import { authRoutes } from '@/Auth/AuthRoutes'
import { profileRoutes } from '@/Profile/ProfileRoutes'
import { pollsRoutes } from '@/Polls/PollsRoutes'

let routes = []

routes = routes.concat(
  appRoutes,
  authRoutes,
  profileRoutes,
  pollsRoutes
)

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
