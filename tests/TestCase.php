<?php

namespace Tests;

use App\Models\Auth\Admin;
use App\Models\Auth\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Mail;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        Mail::fake();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @param array $attributes
     *
     * @return User
     */
    public function actingAsUser(array $attributes = [])
    {
        $user = factory(User::class)->create($attributes);

        $this->actingAs($user);

        return $user;
    }

    /**
     * @param array $attributes
     *
     * @return Admin
     */
    public function actingAsAdmin(array $attributes = [])
    {
        $admin = factory(Admin::class)->create($attributes);

        $this->actingAs($admin);

        return $admin;
    }
}

/**
 * @param $model
 * @param int $number
 * @param array $attributes
 *
 * @return mixed
 */
function create($model, array $attributes = [], $number = null)
{
    return factory($model, $number)->create($attributes);
}

/**
 * @param $model
 * @param int $number
 * @param array $attributes
 *
 * @return array
 */
function make($model, array $attributes = [], $number = null)
{
    return factory($model, $number)->make($attributes)->toArray();
}
