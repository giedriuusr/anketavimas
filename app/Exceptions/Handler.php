<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson()) {
            if ($exception instanceof ValidationException)
                return response()->json(['message' => __('The given data was invalid.'), 'errors' => $exception->validator->getMessageBag()], 422);

            if ($exception instanceof UserCreditException)
                return response()->json([
                    'message' => $exception->getMessage()
                ], 422);

            if ($exception instanceof AttachmentsValidationException)
                return response()->json([
                    'message' => $exception->getMessage()
                ], 422);

            if ($exception instanceof StatusValidationException)
                return response()->json([
                    'message' => $exception->getMessage()
                ], 422);

            if ($exception instanceof TransactionException)
                return response()->json([
                    'message' => $exception->getMessage()
                ], 422);
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param AuthenticationException $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $request->expectsJson()
            ? response()->json(['message' => __($exception->getMessage())], 401)
            : redirect()->guest($exception->redirectTo() ?? route('login'));
    }
}
