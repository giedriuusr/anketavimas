<?php

namespace App\Http\Controllers;

class VueInitController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function vueInit()
    {
        return view('vueInit');
    }
}
