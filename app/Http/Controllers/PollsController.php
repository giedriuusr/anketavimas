<?php

namespace App\Http\Controllers;

use App\Http\Requests\PollResultRequest;
use App\Models\PollResultAnswer;
use App\Models\Poll;
use App\Services\DatatableQuery;
use Illuminate\Http\Request;

class PollsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $polls = Poll::query()->where('status', 'active');

        $query = (new DatatableQuery($request, $polls));

        return response()->json($query->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function show($slug)
    {
        /** @var Poll $poll */
        $poll = Poll::query()->where('url', $slug)->firstOrFail();

        $poll->increment('views_count');

        $poll->load('questions.options');

        return response()->json($poll);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PollResultRequest $request
     * @param                                       $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PollResultRequest $request, $slug)
    {
        /** @var Poll $poll */
        $poll = Poll::query()->where('url', $slug)->with('questions')->firstOrFail();

        $answers = [];

        /** @var \App\Models\PollQuestion $question */
        foreach ($poll->questions as $question) {
            if (empty($request->input("answers.{$question->id}"))) continue;

            if ($question->type == 'checkbox') {
                foreach ($request->input("answers.{$question->id}") as $answer) {
                    $answers[] = new PollResultAnswer([
                        'question_id' => $question->id,
                        'option_id' => $answer,
                    ]);
                }
            } elseif ($question->type == 'radio') {
                $answers[] = new PollResultAnswer([
                    'question_id' => $question->id,
                    'option_id' => $request->input("answers.{$question->id}"),
                ]);
            } else {
                $answers[] = new PollResultAnswer([
                    'question_id' => $question->id,
                    'value' => $request->input("answers.{$question->id}"),
                ]);
            }
        }

        $poll->results()->create()->answers()->saveMany($answers);

        return response()->json([
            'message' => 'Anketa sėkmingai atsakyta.',
        ]);
    }
}
