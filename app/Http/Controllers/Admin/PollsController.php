<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PollsRequest;
use App\Http\Controllers\Controller;
use App\Models\Poll;
use App\Services\DatatableQuery;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PollsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $polls = Poll::query();

        $query = (new DatatableQuery($request, $polls));

        return response()->json($query->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PollsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PollsRequest $request)
    {
        /** @var Poll $poll */
        $poll = Poll::query()->create($request->validated());
        $poll->createOrUpdateQuestions($request->input('questions'));

        return response()->json([
            'message' => 'Anketa sėkmingai sukurta.',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Poll $poll
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Poll $poll)
    {
        $poll->load('questions.answers', 'questions.options');

        return response()->json($poll);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PollsRequest  $request
     * @param  \App\Models\Poll $poll
     * @return \Illuminate\Http\Response
     */
    public function update(PollsRequest $request, Poll $poll)
    {
        $poll->update([
            'title' => $request->input('title'),
            'status' => $request->input('status'),
            'starts_at' => $request->input('starts_at'),
            'ends_at' => $request->input('ends_at'),
        ]);

        $poll->createOrUpdateQuestions($request->input('questions'));

        return response()->json([
            'message' => 'Anketa sėkmingai atnaujinta.',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Poll $poll
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Poll $poll)
    {
        $poll->delete();

        return response('', 204);
    }
}
