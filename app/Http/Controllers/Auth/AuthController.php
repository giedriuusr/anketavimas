<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UserProfileRequest;
use App\Http\Resources\UserResource;
use App\Models\Auth\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except(['login', 'refresh']);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function user()
    {
        /** @var User $user */
        $user = auth()->user();

        return response()->json(['data' => new UserResource($user)]);
    }

    /**
     * @param UserProfileRequest $request
     *
     * @return Authenticatable|null
     */
    public function update(UserProfileRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $user->update($request->validated());

        return response()->json([
            'message' => __('Profile updated successfully.')
        ], 200);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh(false, true));
    }

    /**
     * @param ChangePasswordRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();

        if(! Hash::check($request->input('old_password'), $user->password)) {
            throw ValidationException::withMessages([
                'old_password' => [ __('The current password is wrong.') ],
            ]);
        }

        $user->update([
            'password' => bcrypt($request->input('password'))
        ]);

        return response()->json([
            'message' => __('Password changed successfully.')
        ], 200);
    }

    /**
     * @param Request $request
     * @param Authenticatable $user\
     */
    public function locale(Request $request, Authenticatable $user)
    {
        $user->update([
            'locale' => $request->input('locale'),
        ]);

        App::setLocale($request->input('locale'));
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
