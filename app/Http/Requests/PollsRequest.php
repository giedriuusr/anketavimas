<?php

namespace App\Http\Requests;

use App\Models\Poll;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PollsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'status' => ['required', 'string', Rule::in([
                    Poll::STATUS_DRAFT,
                    Poll::STATUS_ACTIVE,
                    Poll::STATUS_INACTIVE,
                    Poll::STATUS_ENDED,
                ])
            ],
            'starts_at' => 'required|date',
            'ends_at' => 'required|date|after:starts_at',
            'questions' => 'required',
            'questions.*.type' => 'required',
            'questions.*.title' => 'required|string|max:255',
            'questions.*.options.*.value' => 'required',
        ];
    }
}
