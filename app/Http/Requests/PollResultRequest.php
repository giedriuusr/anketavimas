<?php

namespace App\Http\Requests;

use App\Models\Poll;
use App\Models\PollQuestion;
use Illuminate\Foundation\Http\FormRequest;

class PollResultRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $slug = $this->route()->parameter('slug');

        /** @var Poll $poll */
        $poll = Poll::query()->where('url', $slug)->with('questions')->firstOrFail();

        $rules = [];

        foreach($poll->questions as $question) {
            if ($question->required) {
                $rules['answers.'. $question->id] = 'required';
            }
        }

        return $rules;
    }
}
