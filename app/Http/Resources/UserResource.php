<?php

namespace App\Http\Resources;

use App\Models\Auth\Admin;
use App\Models\Auth\User;
use App\Models\Order;
use App\Models\Priority;
use App\Models\Ticket;
use App\Models\Transaction;
use App\Services\OrderStatuses\Submitted;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $user */
        $user = $this->resource;

        return [
            'id' => $user->id,
            'email' => $user->email,
            'firstName' => $user->first_name,
            'lastName' => $user->last_name,
            'fullName' => "{$user->first_name} {$user->last_name}",
            'isAdmin' => $user->type === Admin::class,
            'type' => __(class_basename($user->type)),
            'isVerified' => !!$user->email_verified_at,
        ];
    }
}
