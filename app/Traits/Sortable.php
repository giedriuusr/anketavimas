<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Sortable
{
    /**
     * @param Builder $builder
     * @param $sortBy
     * @param $orderBy
     *
     * @return Builder
     */
    public function scopeSortBy(Builder $builder, $sortBy, $orderBy) {
        // Field name usually comes in camelCase
        $sortBy = snake_case($sortBy);

        if (property_exists($this, 'sortableColumns') && in_array($sortBy, $this->sortableColumns)) {
            return $builder->orderBy($sortBy, $orderBy ? $orderBy : 'desc');
        }

        return $builder;
    }
}
