<?php

namespace App\Services;

use Illuminate\Translation\FileLoader as BaseFileLoader;

class FileLoader extends BaseFileLoader
{
    /**
     * Load a local namespaced translation group for overrides.
     *
     * @param  array $lines
     * @param  string $locale
     * @param  string $group
     * @param  string $namespace
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function loadNamespaceOverrides(array $lines, $locale, $group, $namespace)
    {
        $file = "{$this->path}/vendor/{$namespace}/{$locale}/{$group}.json";

        if ($this->files->exists($file)) {
            return array_replace_recursive($lines, json_decode($this->files->get($file), true));
        }

        return $lines;
    }

    /**
     * Load a locale from a given path.
     *
     * @param  string $path
     * @param  string $locale
     * @param  string $group
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function loadPath($path, $locale, $group)
    {
        if ($this->files->exists($full = "{$path}/{$locale}/{$group}.json")) {
            return json_decode($this->files->get($full), true);
        }

        return [];
    }
}
