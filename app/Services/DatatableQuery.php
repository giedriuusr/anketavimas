<?php

namespace App\Services;

use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class DatatableQuery
{
    private $perPage = 10;

    /** @var Request */
    private $request;

    /** @var Builder */
    private $query;

    /**
     * SearchQuery constructor.
     *
     * @param Request $request
     * @param $query
     */
    public function __construct(Request $request, $query)
    {
        $this->request = $request;
        $this->query = $query;
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function get()
    {
        $this->sort();

        return $this->paginate();
    }

    /**
     * @return $this
     */
    private function sort()
    {
        if ($this->request->has('sort')) {
            $this->query->sortBy(
                $this->request->query('sort'),
                $this->request->query('order')
            );
        }

        return $this;
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    private function paginate()
    {
        $perPage = $this->request->query('per_page', $this->perPage);

        return $this->query->paginate($perPage);
    }
}
