<?php

namespace App\Models\Auth;

use App\Notifications\VerifyEmail;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property mixed id
 * @property string email
 * @property string first_name
 * @property string last_name
 * @property string type
 * @property Carbon email_verified_at
 */
class Authenticatable extends User implements JWTSubject
{
    /** @var string */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'type',
        'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Determines if user is of type Admin.
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this instanceof Admin;
    }

    /**
     * Determines if user is of type User.
     *
     * @return bool
     */
    public function isUser()
    {
        return $this instanceof \App\Models\Auth\User;
    }

    /**
     * @return $this
     */
    public function hydrateToRealModel()
    {
        /** @var \Illuminate\Database\Eloquent\Model $subclass */
        $type = $this->type;

        if ($this instanceof $type) {
            return $this;
        }

        /** @var \App\Models\Auth\Authenticatable $user */
        $user = new $type(auth()->user()->toArray());
        $user->exists = true;
        $user->{$user->getKeyName()} = $this->getKey();

        return $user;
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }
}
