<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string type
 *
 * @property-read \App\Models\Poll poll
 * @property-read \Illuminate\Support\Collection options
 */
class PollQuestion extends Model
{
    const TYPE_RADIO = 'radio';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_TEXT = 'text';
    const TYPE_TEXTAREA = 'textarea';

    /**
     * @var array
     */
    protected $fillable = [
        'poll_id',
        'title',
        'type',
        'required',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'answers',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function poll()
    {
        return $this->belongsTo(Poll::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany(PollQuestionOption::class, 'question_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(PollResultAnswer::class, 'question_id');
    }

    /**
     * @param array $options
     */
    public function createOrUpdateQuestionOptions(array $options)
    {
        foreach ($options as $option) {
            $this->options()->updateOrCreate(['id' => $option['id'] ?? null], $option);
        }
    }

    /**
     * @return null|array
     */
    public function getAnswersAttribute()
    {
        return $this->type == self::TYPE_CHECKBOX ? [] : null;
    }
}
