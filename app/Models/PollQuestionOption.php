<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property \App\Models\PollQuestion question
 */
class PollQuestionOption extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['value'];

    /** @var array */
    protected $appends = [
        'answers_count',
    ];

    /**

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(PollQuestion::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(PollResultAnswer::class, 'option_id');
    }

    /**
     * @return int
     */
    public function getAnswersCountAttribute()
    {
        return $this->answers()->count();
    }
}
