<?php

namespace App\Models;

use App\Traits\Sortable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * @property mixed      id
 * @property string status
 * @property string      url
 * @property string ends_at
 *
 * @property-read int        answers_count
 * @property-read Collection questions
 * @property string status_text
 */
class Poll extends Model
{
    use SoftDeletes, Sortable;

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_DRAFT = 'draft';
    const STATUS_ENDED = 'ended';

    public $sortableColumns = [
      'status',
      'ends_at',
      'answers_count',
    ];

    /**
     * @var array
     */
    protected $guarded = ['questions'];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'status',
        'views_count',
        'answers_count',
        'type',
        'url',
        'starts_at',
        'ends_at',
    ];

    /**
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    protected $appends = [
        'full_url',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function (Poll $model) {
            $model->url = $model->generateSlug();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(PollQuestion::class, 'poll_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function results()
    {
        return $this->hasMany(PollResult::class, 'poll_id');
    }

    /**
     * @return null|string
     */
    public function getFullUrlAttribute()
    {
        return $this->url ? route('polls.show', ['slug' => $this->url]) : null;
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        return $this->isEnded() ? self::STATUS_ENDED : $this->status;
    }

    /**
     * @return bool
     */
    public function isEnded()
    {
        return Carbon::now()->toDateString() > $this->ends_at;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status == 'active' && ! $this->isEnded();
    }

    /**
     * @return bool
     */
    public function isDraft(): bool
    {
        return $this->status == 'draft';
    }

    /**
     * @param array $questions
     */
    public function createOrUpdateQuestions(array $questions)
    {
        $this->questions()->delete();

        foreach ($questions as $question) {
            /** @var PollQuestion $questionObj */
            $questionObj = $this->questions()->updateOrCreate(['id' => $question['id'] ?? null], $question);
            $questionObj->createOrUpdateQuestionOptions(array_get($question, 'options', []));
        }
    }

    /**
     * @return string
     */
    private function generateSlug()
    {
        $slug = str_random(16);

        if (self::query()->where('url', $slug)->exists()) {
            return $this->generateSlug();
        }

        return $slug;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array                                 $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApplyQueryFilters(Builder $query, array $filters = [])
    {
        if (empty($filters)) return $query;

        foreach ($filters as $filter => $value) {
            if($filter == 'q') {
                $query->searchByName($value);
            }
        }

        return $query;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $search
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByName(Builder $query, $search)
    {
        return $query->where(function (Builder $q) use ($search) {
            return $q->where('title', 'LIKE', "%{$search}%")
                ->orWhere('ends_at', 'LIKE', "%{$search}%");
        });
    }
}
