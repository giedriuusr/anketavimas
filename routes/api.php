<?php

$this->post('/register', 'Auth\RegisterController@register')->name('register');
$this->post('/auth/refresh', 'Auth\AuthController@refresh')->name('auth.refresh');
$this->post('/auth/login', 'Auth\LoginController@login')->name('login');
$this->post('/password-reset', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.reset.mail');
$this->get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');

$this->group(['middleware' => 'auth'], function () {
    $this->get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
    $this->get('/auth/user', 'Auth\AuthController@user')->name('auth.user');
    $this->put('/auth/profile', 'Auth\AuthController@update')->name('auth.update');
    $this->put('/auth/change-password', 'Auth\AuthController@changePassword')->name('auth.change-password');

    $this->apiResource('polls', 'PollsController');
    $this->apiResource('/admin/polls', 'Admin\PollsController', [
        'as' => 'admin',
    ]);

    $this->get('/poll/{slug}', 'PollsController@show')->name('poll.show');
    $this->post('/poll/{slug}', 'PollsController@store')->name('poll.store');
});


