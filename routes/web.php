<?php

/** Auth */
$this->get('/password-reset/success','Auth\ResetPasswordController@success')->name('password.reset.successful');
$this->get('/password-reset/{token}','Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('/password-request','Auth\ResetPasswordController@reset')->name('password.request');

$this->get('{any}', 'VueInitController@vueInit')->name('vueInit')->where('any', '.*');

