const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './resources/js/modules'),
      '%': path.resolve(__dirname, './resources/js/services'),
      '+': path.resolve(__dirname, './resources/js/models'),
      '*': path.resolve(__dirname, './resources/js')
    }
  },
  plugins: [
    new VueLoaderPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  }
}
